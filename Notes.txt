

Vertex painting // ideas

1. Limit to materials with shader that uses vertex channel EXCEPT default-diffuse, where shader of choice will be assigned.


Modes:

-Vertex-at-a-time (pick closest vertex, including surrounding tris)
-Triangle-at-a-time (paint full triangle, ignoring barycenter)
-Flood fill


-Triangle-at-a-time, using barycenter (current strategy)

